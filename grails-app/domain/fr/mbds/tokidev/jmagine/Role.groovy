package fr.mbds.tokidev.jmagine

class Role {

	String 		authority
	Integer		level

	static mapping = {
		cache true
	}

	static constraints = {
		authority 	blank: false, unique: true
		level		unique: true
	}
}
