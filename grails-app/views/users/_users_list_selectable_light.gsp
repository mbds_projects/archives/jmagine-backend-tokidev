<div class="row users_list">
    <div class="col-xs-12 col-sm-12">
        <g:each in="${ users }" var="user">
            <div class='user selectable <g:if test="${parcours.moderators.contains(user)}">selected</g:if>'>
                <div class="checkicon glyphicon glyphicon-ok">
                </div>
                <div class="thumb img-circle">
                    <g:jmagineImage src="${user.thumbnail?.filename}"/>
                </div>
                    <g:include view="users/_user_body.gsp" model="[user:user]"/>
            </div>
        </g:each>
    </div>
</div>
<script type="text/javascript">
    $(function()
    {
        $(".user.selectable").click(function()
        {
            var url;
            if ($(this).hasClass("selected"))
            {
                $(this).removeClass('selected');
                url = '${createLink(controller: 'moderators', action: 'do_remove', params:['p_id':parcours.id,'user_id':"user_id"])}';
                url = url.replace('user_id',$(this).find('.username').data('user-id'));
            }
            else
            {
                $(this).addClass('selected');
                url = '${createLink(controller: 'moderators', action: 'do_add', params:['p_id':parcours.id,'user_id':"user_id"])}';
                url = url.replace('user_id',$(this).find('.username').data('user-id'));
            }
            $.ajax(url).always(function(data,status,error)
            {
                popOutModAlert(data,status);
            });

            function popOutModAlert(data,status)
            {
                var div_alert = $('#mod_alert');
                switch (status)
                {
                    case "success":
                        div_alert.html('' +
                        '<div class="alert alert-success" id="success-alert">'+data+'</div>');
                        $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                            $("#success-alert").alert('close');
                        });
                        break;
                    case "error":
                        div_alert.html('' +
                        '<div class="alert alert-danger" id="error-alert">'+data.responseText+'</div>');
                        $("#error-danger").fadeTo(2000, 500).slideUp(500, function(){
                            $("#error-danger").alert('close');
                        });
                        break;
                }
            }
        });
    });
</script>