package fr.mbds.tokidev.jmagine

import grails.transaction.Transactional
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.security.acl.Owner

@Transactional
class ParcoursService {
    def springSecurityService
    ImageService imageService

    Parcours createParcours( params ) {
        FileContainer background
        Parcours new_parcours = new Parcours(
                title:params.title,
                description:params.description,
                author:params.author,
        ).addToModerators( params.author ).save(failOnError:true)

        if( params.imageType == 'upload' && params.background_picture && !params.background_picture.empty ) {
            background = imageService.createImage( ownerType:OwnerType.BACKEND, ownerId:params.author.id, picture:params.background_picture, sizing_informations: [width:770, height:513] )
            new_parcours.addToFileList( background )
            new_parcours.backgroundPic = background
        }

        return new_parcours.save(failOnError: true, flush:true)
    }

    Parcours updateParcours( params ) {
        Parcours parcours = params.parcours
        parcours.title = params.title;
        parcours.description = params.description;
        if( params.imageType == 'empty' ) parcours.backgroundPic = null;
        else if( ( params.imageType == 'librairy' ) && ( params.imageId != null ) ) {
            FileContainer media = FileContainer.get( params.imageId )
            if( media ) parcours.backgroundPic = media
        }
        else if( ( params.imageType == 'upload' ) && params.background_picture && !params.background_picture.empty ) {
            parcours.backgroundPic = imageService.createImage( ownerType:OwnerType.BACKEND, ownerId:parcours.author.id, picture:params.background_picture, sizing_informations: [width:770, height:513] )
            parcours.addToFileList( parcours.backgroundPic )
        }
        if( !parcours.backgroundPic || !parcours.pois.size() ) parcours.isValidated = false;
        return parcours.save()
    }

    boolean toogleParcoursStatus( Parcours parcours, boolean state ) {
        if( state && parcours.backgroundPic && parcours.pois.size() ) parcours.isValidated = true;
        else parcours.isValidated = false;
        parcours.save()
        return parcours.isValidated
    }

    ContentComponent addComponent( params ) {
        Parcours parcours = params.parcours;
        FileContainer background

        if( ( params.imageType == 'librairy' ) && ( params.imageId != null ) ) {
            background = FileContainer.get( params.imageId )
        }
        else if( ( params.imageType == 'upload' ) && params.background_picture && !params.background_picture.empty ) {
            background = imageService.createImage( ownerType:OwnerType.BACKEND, ownerId:parcours.author.id, picture:params.background_picture, sizing_informations: [width:770, height:513] )
            parcours.addToFileList( background )
            parcours.save( flush:true )
        }

        ContentComponent component = new ContentComponent( title:params.title,
                content:params.content, backgroundPic: background )
        parcours.addToComponents( component )
        component.save()
        parcours.save()
        return component
    }

    Boolean editComponent( params ) {
        def sectionInstance = params.section
        sectionInstance.title = params.title
        sectionInstance.content = params.content
        def parcoursInstance = params.parcours
        if( ( params.imageType == 'librairy' ) && ( params.imageId != null ) ) {
            FileContainer media = FileContainer.get( params.imageId )
            if( media ) sectionInstance.backgroundPic = media
        }
        else if( ( params.imageType == 'upload' ) && params.background_picture && !params.background_picture.empty ) {
            sectionInstance.backgroundPic = imageService.createImage( ownerType:OwnerType.BACKEND, ownerId:parcoursInstance.author.id, picture:params.background_picture, sizing_informations: [width:770, height:513] )
            parcoursInstance.addToFileList( sectionInstance.backgroundPic )
        }
        parcoursInstance.save()
        sectionInstance.save()
        return true
    }
}
